<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project aedev.aedev V0.3.24 -->
<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project aedev.tpl_namespace_root V0.3.14 -->
# git_repo_manager 0.3.87

[![GitLab develop](https://img.shields.io/gitlab/pipeline/aedev-group/aedev_git_repo_manager/develop?logo=python)](
    https://gitlab.com/aedev-group/aedev_git_repo_manager)
[![LatestPyPIrelease](
    https://img.shields.io/gitlab/pipeline/aedev-group/aedev_git_repo_manager/release0.3.86?logo=python)](
    https://gitlab.com/aedev-group/aedev_git_repo_manager/-/tree/release0.3.86)
[![PyPIVersions](https://img.shields.io/pypi/v/aedev_git_repo_manager)](
    https://pypi.org/project/aedev-git-repo-manager/#history)

>aedev_git_repo_manager package 0.3.87.

[![Coverage](https://aedev-group.gitlab.io/aedev_git_repo_manager/coverage.svg)](
    https://aedev-group.gitlab.io/aedev_git_repo_manager/coverage/index.html)
[![MyPyPrecision](https://aedev-group.gitlab.io/aedev_git_repo_manager/mypy.svg)](
    https://aedev-group.gitlab.io/aedev_git_repo_manager/lineprecision.txt)
[![PyLintScore](https://aedev-group.gitlab.io/aedev_git_repo_manager/pylint.svg)](
    https://aedev-group.gitlab.io/aedev_git_repo_manager/pylint.log)

[![PyPIImplementation](https://img.shields.io/pypi/implementation/aedev_git_repo_manager)](
    https://gitlab.com/aedev-group/aedev_git_repo_manager/)
[![PyPIPyVersions](https://img.shields.io/pypi/pyversions/aedev_git_repo_manager)](
    https://gitlab.com/aedev-group/aedev_git_repo_manager/)
[![PyPIWheel](https://img.shields.io/pypi/wheel/aedev_git_repo_manager)](
    https://gitlab.com/aedev-group/aedev_git_repo_manager/)
[![PyPIFormat](https://img.shields.io/pypi/format/aedev_git_repo_manager)](
    https://pypi.org/project/aedev-git-repo-manager/)
[![PyPILicense](https://img.shields.io/pypi/l/aedev_git_repo_manager)](
    https://gitlab.com/aedev-group/aedev_git_repo_manager/-/blob/develop/LICENSE.md)
[![PyPIStatus](https://img.shields.io/pypi/status/aedev_git_repo_manager)](
    https://libraries.io/pypi/aedev-git-repo-manager)
[![PyPIDownloads](https://img.shields.io/pypi/dm/aedev_git_repo_manager)](
    https://pypi.org/project/aedev-git-repo-manager/#files)


## installation


execute the following command to install the
aedev.git_repo_manager package
in the currently active virtual environment:
 
```shell script
pip install aedev-git-repo-manager
```

if you want to contribute to this portion then first fork
[the aedev_git_repo_manager repository at GitLab](
https://gitlab.com/aedev-group/aedev_git_repo_manager "aedev.git_repo_manager code repository").
after that pull it to your machine and finally execute the
following command in the root folder of this repository
(aedev_git_repo_manager):

```shell script
pip install -e .[dev]
```

the last command will install this package portion, along with the tools you need
to develop and run tests or to extend the portion documentation. to contribute only to the unit tests or to the
documentation of this portion, replace the setup extras key `dev` in the above command with `tests` or `docs`
respectively.

more detailed explanations on how to contribute to this project
[are available here](
https://gitlab.com/aedev-group/aedev_git_repo_manager/-/blob/develop/CONTRIBUTING.rst)


## namespace portion documentation

information on the features and usage of this portion are available at
[ReadTheDocs](
https://aedev.readthedocs.io/en/latest/_autosummary/aedev.git_repo_manager.html
"aedev_git_repo_manager documentation").
